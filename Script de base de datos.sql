-- Base de datos para los registros codificados con Csharp
CREATE DATABASE UsuariosCsharp
USE UsuariosCsharp

CREATE TABLE Usuarios(
id int PRIMARY KEY,
Usuario nvarchar(300),
Contrasenia nvarchar(300)
)

-- Base de datos para los registros encriptados con SQL Server.
CREATE DATABASE UsuariosBaseDeDatos
USE UsuariosBaseDeDatos

CREATE TABLE Usuarios(
id int PRIMARY KEY,
Usuario nvarchar(300),
Contrasenia nvarchar(300)
)