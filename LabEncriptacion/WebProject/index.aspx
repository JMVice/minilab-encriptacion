﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="WebProject.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <title></title>
</head>
<body>
    <div class="container body-content">

        <form id="form1" runat="server">
            <div class="jumbotron">
                <h1>Lab de encriptacion</h1>
                <hr />
            </div>
            <h2>Encriptacion en C#</h2>
            <p>
                Ingrese un nombre de usuario y contraseña. La encriptación de este
                formulario se hará mediante C#
                <br />
                <label>Nombre de usuario:</label>
                <asp:TextBox ID="nombreUsuario_Csharp" runat="server"></asp:TextBox>
                <br />
                <label>Contraseña:</label>
                <asp:TextBox ID="contrasenia_Csharp" runat="server"></asp:TextBox>
                <br />
                <asp:Button ID="enviarDatos_Csharp" runat="server" Text="Enviar datos" OnClick="enviarDatos_Csharp_Click" />
                <br />
            </p>
            <hr />
            <h2>Encriptacion en la base de datos</h2>
            <p>
                Ingrese un nombre de usuario y contraseña. La encriptación de este
                formulario se hará mediante la base de datos SQL Server.
                <br />
                <label>Nombre de usuario:</label>
                <asp:TextBox ID="nombreUsuario_sqlServer" runat="server"></asp:TextBox>
                <br />
                <label>Contraseña:</label>
                <asp:TextBox ID="contrasenia_sqlServer" runat="server"></asp:TextBox>
                <br />
                <asp:Button ID="enviarDatos_sqlServer" runat="server" Text="Enviar datos" OnClick="enviarDatos_sqlServer_Click" />
            </p>

        </form>
        <footer>Lab encriptacion - ULACIT 2020</footer>
    </div>
</body>
</html>
