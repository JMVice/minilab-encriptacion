﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace WebProject.Logica
{
    public class SQLogic : SQL
    {
        // Conexion de datos para base de datos con registros codificados con C#.
        string stringDeConnection_Csharp = ConfigurationManager.ConnectionStrings["csharp_database"].ConnectionString;

        // Conexion de datos para base de datos con registros encriptados por base de datos.
        string stringDeConnection_baseDeDatos = "Data Source = 10.172.61.115; Initial Catalog = UsuariosCsharp; User ID = admin; Password=***********";

        public void guardarUsuario_codificacionCsharp(string usuario, string contrasenia)
        {
            string query = @"USE UsuariosCsharp
                             INSERT INTO Usuarios (Usuario, Contrasenia)
                             VALUES(" + usuario + ", " + contrasenia + ")" ;
            Query(query, stringDeConnection_Csharp);
        }

        public void guardarUsuario_codificacionBaseDeDatos(string usuario, string contrasenia)
        {
            string query = "";
            Query(query, stringDeConnection_baseDeDatos);
        }
    }
}