﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace WebProject.Logica
{
    public class EncriptacionBase64
    {
        /// <summary>
        /// Metodo para codificar
        /// el parametro cadena nos da el valor para poder encriptar
        /// </summary>
        /// <param name="cadena"></param>
        /// <returns></returns>
        public string encriptar(string cadena)
        {
            string resultado = string.Empty;
            Byte[] encriptar = new UnicodeEncoding().GetBytes(cadena);
            resultado = Convert.ToBase64String(encriptar);
            return resultado;
        }

        public string desencriptar(string cadena)
        {
            string resultado = string.Empty;
            Byte[] descriptar = Convert.FromBase64String(cadena);
            resultado = new UnicodeEncoding().GetString(descriptar);
            return resultado;
        }
    }
}