﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace WebProject.Logica
{
    public class SQL
    {
        public void Query(string query, string connectionString)
        {
            SqlConnection connection = new SqlConnection(connectionString);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = query;
            cmd.Connection = connection;

            connection.Open();
            cmd.ExecuteNonQuery();
            connection.Close();
        }

        public DataSet Fetch(string query, string connectionString)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlDataAdapter dadapter = new SqlDataAdapter(query, connection);
            DataSet ds = new DataSet();
            connection.Open();
            dadapter.Fill(ds);
            connection.Close();
            return ds;
        }
    }
}
