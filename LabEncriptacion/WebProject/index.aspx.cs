﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebProject.Logica;

namespace WebProject
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }



        protected void enviarDatos_sqlServer_Click(object sender, EventArgs e)
        {
            // Capturamos la información ingresada por el usuario y la guardamos
            // en dos variables string con motivos de organización.
            string nombreDeUsuario_input = nombreUsuario_Csharp.Text;
            string contrasenia_input = contrasenia_Csharp.Text;

            // Enviamos la información en texto plano a la base de datos. La base de datos
            // se encarga de hacer la encriptación.
            // Aqui va codigo SQL...
        }

        // Boton para enviar informacion codificada a la base de datos mediante C#.
        protected void enviarDatos_Csharp_Click(object sender, EventArgs e)
        {
            // Declaración de clase de codificación Base64.
            EncriptacionBase64 base64 = new EncriptacionBase64();

            // Capturamos la información ingresada por el usuario y la codificamos
            // en Base64.
            string nombreDeUsuario_codificado = base64.encriptar(nombreUsuario_Csharp.Text);
            string contrasenia_codificado = base64.encriptar(contrasenia_Csharp.Text);

            // Enviamos la información codificada en Base64 a la base de datos.
            SQLogic sql = new SQLogic();
            sql.guardarUsuario_codificacionCsharp(nombreDeUsuario_codificado, contrasenia_codificado);
        }
    }
}